export const environment = {
  dev: true,
  stage: false,
  production: false,
  databaseName: "schemas-dev",
  version: '1.0.0-dev-alpha',
  firebaseConfig: {
    apiKey: "AIzaSyBOcet8l-x_SzHc3p-rgmA5f3xrXXD-aHY",
    authDomain: "everi-mobile-build-framework.firebaseapp.com",
    databaseURL: "https://everi-mobile-build-framework-default-rtdb.firebaseio.com",
    projectId: "everi-mobile-build-framework",
    storageBucket: "everi-mobile-build-framework.appspot.com",
    messagingSenderId: "603769724235",
    appId: "1:603769724235:web:6cbc29b71df4547fe89cce",
    measurementId: "G-CYX7KMJYSB"
  }
};
