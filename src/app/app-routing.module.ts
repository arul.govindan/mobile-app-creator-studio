import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './guard/auth/auth.guard';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'registration',
    loadChildren: () => import('./pages/modals/registration/registration.module').then( m => m.RegistrationPageModule),
    canActivate:[AuthGuard]
  },
  {
    path: 'verify-email',
    loadChildren: () => import('./pages/modals/verify-email/verify-email.module').then( m => m.VerifyEmailPageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'dashboard',
    loadChildren: () => import('./pages/dashboard/dashboard.module').then( m => m.DashboardPageModule),
    canActivate:[AuthGuard]
  },
  {
    path: 'password-reset',
    loadChildren: () => import('./pages/modals/password-reset/password-reset.module').then( m => m.PasswordResetPageModule)
  },
  {
    path: 'app-creator-form',
    loadChildren: () => import('./pages/app-creator-form/app-creator-form.module').then( m => m.AppCreatorFormPageModule),
    canActivate:[AuthGuard]
  },
  {
    path: 'convert-css-json',
    loadChildren: () => import('./pages/modals/convert-css-json/convert-css-json.module').then( m => m.ConvertCssJsonPageModule),
    canActivate:[AuthGuard]
  },
  {
    path: 'app-configuration-preview',
    loadChildren: () => import('./pages/modals/app-configuration-preview/app-configuration-preview.module').then( m => m.AppConfigurationPreviewPageModule),
    canActivate:[AuthGuard]
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
