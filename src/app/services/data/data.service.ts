import { Injectable } from '@angular/core';
import { User } from '../../models/user';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/compat/database';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class DataService {
  userDetails: User | undefined;
  appCreatorStudioSchema: any;
  appsCreatedList: any[] | undefined;
  databaseBasePath: string = `app-create-studio/${environment.databaseName}`;

  // Firebase Path Variables Section
  appsListPath: string = `${this.databaseBasePath}/app/appsList`;
  appsThemePath: string = `${this.databaseBasePath}/app/appThemes`;

  constructor(private db: AngularFireDatabase) { }
  /**
   * --------------------------------------------------------------------------------
   * Firebase Real Time Database (RTDB) Section Starts
   * --------------------------------------------------------------------------------
   */
  public getAppCreatorStudioSchema(): Observable<any> {
    this.appCreatorStudioSchema = this.db.list(this.databaseBasePath);
    return this.appCreatorStudioSchema.valueChanges();
  }
  public getThemeSchema(): Observable<any> {
    const default_theme_schema_config: any = this.db.list(this.appsThemePath);
    return default_theme_schema_config.valueChanges();
  }
  public getCreatedAppsList(): Observable<any> {
    let firebaseAppsList: any = this.db.list(this.appsListPath);
    return firebaseAppsList.valueChanges();
  }
  public updateCreatedAppsList(appInfoFinalJson: object): Observable<any> {
    let firebaseAppsListDb: any = this.db.list(this.appsListPath);
    if (firebaseAppsListDb && appInfoFinalJson) {
      firebaseAppsListDb.push(appInfoFinalJson);
    }
    return firebaseAppsListDb.valueChanges();
  }
  public deleteApp(updatedAppsList: any): Observable<any> {
    let firebaseAppsListDb: AngularFireList<any> = this.db.list(this.appsListPath);
    if (firebaseAppsListDb && updatedAppsList) {
      firebaseAppsListDb.remove();
      firebaseAppsListDb.update("", updatedAppsList);
    }
    return firebaseAppsListDb.valueChanges();
  }
  /**
   * --------------------------------------------------------------------------------
   * Firebase Real Time Database (RTDB) Section Ends
   * --------------------------------------------------------------------------------
   */
  // --------------------------------------------------------------------------------
  // --------------------------------------------------------------------------------
  /**
   * --------------------------------------------------------------------------------
   * Local Browser Storage Section Starts
   * --------------------------------------------------------------------------------
   */
  public getUserDetails(): Promise<any> {
    const tempUserData: any = localStorage.getItem('user') ? localStorage.getItem('user') : {};
    const userData: User = JSON.parse(tempUserData);
    return Promise.resolve(userData);
  }
  public setUserDetails(userData: User | null): Promise<void> {
    const tempUserData = typeof userData == 'object' ? JSON.stringify(userData) : userData;
    return Promise.resolve(localStorage.setItem('user', tempUserData));
  }
  /**
   * --------------------------------------------------------------------------------
   * Local Browser Storage Section Ends
   * --------------------------------------------------------------------------------
   */
}
