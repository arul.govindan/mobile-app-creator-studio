import { Component, OnInit } from '@angular/core';
import { User } from '../../models/user';
import { DataService } from '../../services/data/data.service';
import { AuthenticationService } from '../../services/authentication/authentication-service';
import { NavigationExtras, Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { environment } from 'src/environments/environment';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {

  userInformation: User | any;
  appsList: any;
  appsListCount: number | undefined;
  isOpen = false;
  showSkeleton: boolean = true;
  appVersion: string = environment.version;

  dashBoardSections: object[] = [
    {
      "key": "app-count",
      "title": "Total Count",
      "subtitle": "Count of the Created Apps"
    },
    {
      "key": "recent-app",
      "title": "Recent App",
      "subtitle": "Recently Created App"
    }
  ];

  constructor(
    public authService: AuthenticationService,
    public dataService: DataService,
    public router: Router,
    public modal: ModalController
  ) {
  }

  ngOnInit() {
    this.intializeDataSource();
  }

  private intializeDataSource(): void {
    this.getUserInformation();
    this.getAppsList();
  }

  private getUserInformation(): void {
    this.dataService.getUserDetails().then((user: User) => {
      console.log("Dashboard Page", user);
      this.userInformation = user;
    });
  }

  private getAppsList(): void {
    this.dataService.getCreatedAppsList().subscribe((appsListData: any) => {
      this.appsList = appsListData;
      this.appsList = this.appsList.sort((a: any, b: any) => {
        return <any>new Date(b.createdOn) - <any>new Date(a.createdOn);
      });
      console.log("Dashboard appsList", this.appsList);
      this.appsListCount = (appsListData && appsListData.length >= 1) ? appsListData.length : 0;
      console.log("Dashboard appsListCount", this.appsListCount);
      this.showSkeleton = false;
    });
  }

  public createNewApp(): void {
    this.router.navigate(['app-creator-form']);
  }

  public convertTimeStamptoDate(timeStamp: any): any {
    const date = new Date(timeStamp);
    return date.toLocaleDateString();
  }

  public deleteApp(index: number): void {
    if (window.confirm('Are sure you want to delete this app configurations ?')) {
      this.appsList = this.appsList?.slice(0, index);
      this.showSkeleton = true;
      this.dataService.deleteApp(this.appsList).subscribe((appsListData: any) => {
        this.getAppsList();
      });
    }
  }

  public handleSearchChange(event: any): void {
    const query = event.target.value.toLowerCase();
    console.log(query);
    if (query) {
      this.appsList = this.appsList.filter((d: any) => {
        if (
          d.appConfig?.appInfo?.appInformation?.AppName.toLowerCase().indexOf(query) > -1 ||
          d.appConfig?.appInfo?.appInformation?.ProjectName.toLowerCase().indexOf(query) > -1
        )
          return d;
      });
    } else {
      this.handleSearchClear();
    }
  }

  public handleSearchClear(): void {
    this.getAppsList();
  }

  public showAppConfigurationPreview(appListObject: object) {
    let navigationExtras: NavigationExtras = {
      state: {
        appListObject: appListObject
      }
    };
    this.router.navigate(['app-configuration-preview'], navigationExtras);
  }
}

