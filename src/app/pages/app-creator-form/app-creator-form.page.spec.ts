import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AppCreatorFormPage } from './app-creator-form.page';

describe('AppCreatorFormPage', () => {
  let component: AppCreatorFormPage;
  let fixture: ComponentFixture<AppCreatorFormPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(AppCreatorFormPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
