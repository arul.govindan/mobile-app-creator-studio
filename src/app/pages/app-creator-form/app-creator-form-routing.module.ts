import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AppCreatorFormPage } from './app-creator-form.page';

const routes: Routes = [
  {
    path: '',
    component: AppCreatorFormPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AppCreatorFormPageRoutingModule {}
