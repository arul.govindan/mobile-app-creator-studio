import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { AppCreatorFormPageRoutingModule } from './app-creator-form-routing.module';
import { AppCreatorFormPage } from './app-creator-form.page';
import { ConvertCssJsonPage } from '../modals/convert-css-json/convert-css-json.page';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    AppCreatorFormPageRoutingModule
  ],
  declarations: [AppCreatorFormPage, ConvertCssJsonPage]
})
export class AppCreatorFormPageModule {}
