import { Component, ViewChild, OnInit, AfterViewInit } from '@angular/core';
import { User } from '../../models/user';
import { DataService } from '../../services/data/data.service';
import { AuthenticationService } from '../../services/authentication/authentication-service';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { AlertController, PopoverController, ModalController } from '@ionic/angular';
import { Router } from '@angular/router';
import { RegistrationPage } from '../modals/registration/registration.page';
import JSONEditor, { JSONEditorOptions } from 'jsoneditor';
import { Location } from "@angular/common";
import { ConvertCssJsonPage } from '../modals/convert-css-json/convert-css-json.page';


@Component({
  selector: 'app-app-creator-form',
  templateUrl: './app-creator-form.page.html',
  styleUrls: ['./app-creator-form.page.scss'],
})
export class AppCreatorFormPage implements OnInit, AfterViewInit {
  @ViewChild('popover') popover: any | undefined;

  workItemForm: FormGroup | any;
  appInfoForm: FormGroup | any;
  appFeaturesForm: FormGroup | any;
  appFeaturesOptions: boolean[] = [true, false];
  appFastlaneSecretsForm: FormGroup | any;
  appThemesForms: FormGroup | any;
  myForm: FormGroup | any;

  userInformation: User | any;
  appCreatorStudioSchema: any;

  showSkeleton: boolean = true;
  showSkeletonArray: number[] = [1, 2, 3, 4, 5, 6, 7, 8];
  isOpen = false;

  themesResultSchema: any = {};

  jsonEditorOptions: JSONEditorOptions = {};
  jsonEditor: JSONEditor | null = null;

  isEditCreator: boolean = true;

  constructor(
    public authService: AuthenticationService,
    public dataService: DataService,
    public formBuilder: FormBuilder,
    public alert: AlertController,
    public router: Router,
    public popOver: PopoverController,
    public modal: ModalController,
    private location: Location
  ) {
  }

  ngOnInit() {
    setTimeout(() => {
      this.showSkeleton = !this.showSkeleton;
      this.initializeDataSource();
    }, 2000);
  }

  ngAfterViewInit(): void {
    // Adding timeout to load the HTML DOM Elements fully and then bind the values
    setTimeout(() => {
      this.initializeThemesJsonEditor();
    }, 3500);
  }

  public initializeThemesJsonEditor(): void {
    if (this.appCreatorStudioSchema && this.appCreatorStudioSchema?.appThemes?.sections) {
      this.appCreatorStudioSchema?.appThemes?.sections.forEach((element: any) => {
        this.initializeJSONEditor(`${element}`);
      });
    }
  }

  public initializeJSONEditor(sectionName: string) {
    console.log(`Initializing JSON Editor ${sectionName}`);
    let element = window.document.getElementById(`${sectionName}`);
    console.log(`element is ${element}`);
    if (element && this.themesResultSchema) {
      console.log('inside initializeJSONEditor');
      const options: JSONEditorOptions = {
        mode: 'code',
        modes: ['code', 'text', 'tree', 'view'],
        onChange: () => {
          this.appThemesForms.controls[`${sectionName}`].setValue(editor.get());
        }
      };
      let editor = new JSONEditor(element, options)
      editor.set(this.themesResultSchema[0][`${sectionName}`]);
    }
  }


  private initializeDataSource(): void {
    this.dataService.getUserDetails().then((user: User) => {
      console.log("Dashboard Page", user);
      this.userInformation = user;
    });

    this.dataService.getAppCreatorStudioSchema().subscribe((value: any) => {
      console.log("getAppCreatorStudioSchema", value[0]);
      this.appCreatorStudioSchema = value[0];
      if (this.appCreatorStudioSchema) {
        this.initializeForms();
      }
    });

    this.dataService.getThemeSchema().subscribe((schemaResult: object) => {
      console.log('getThemeSchema', schemaResult);
      this.themesResultSchema = schemaResult;
    });
  }

  private initializeForms(): void {
    this.initializeWorkItemForms();
    this.initializeAppInfoForms();
    this.initializeAppFeaturesForms();
    this.initializeAppFastlaneSecrets();
    this.initializeAppThemes();
  }

  public presentPopover(e: Event) {
    this.popover.event = e;
    this.isOpen = true;
  }

  private initializeWorkItemForms(): void {
    let workItemFormObject: any = {};
    this.appCreatorStudioSchema?.appInfo?.workItem.forEach((element: any) => {
      if (element === "ID")
        workItemFormObject[`${element}`] = new FormControl('', [Validators.required, Validators.pattern("^[0-9]*$"), Validators.minLength(5)]);
      else
        workItemFormObject[`${element}`] = new FormControl('', [
          Validators.required,
          Validators.maxLength(1000),
          Validators.pattern("^[ A-Za-z0-9_@.,\/#&+-]*$")
        ]);
      if (element)
        this.workItemForm = new FormGroup(workItemFormObject);
    });
  }

  private initializeAppInfoForms(): void {
    let appInfoFormObject: any = {};
    this.appCreatorStudioSchema?.appInfo?.appInformation.forEach((element: any) => {
      if (element === 'AndroidBundleID' || element === 'iOSBundleID')
        appInfoFormObject[`${element}`] = new FormControl('', [
          Validators.required,
          Validators.minLength(5),
          Validators.pattern("^[a-z][a-z0-9_]*(\.[a-z][a-z0-9_]*)+$")
        ]);
      else
        appInfoFormObject[`${element}`] = new FormControl('', [
          Validators.required,
          Validators.minLength(5),
          Validators.pattern('^[ A-Za-z0-9_@.,\/#&+-]*$')
        ]);
      if (element)
        this.appInfoForm = new FormGroup(appInfoFormObject);
    });
  }

  private initializeAppFeaturesForms(): void {
    let appFeaturesFormObject: any = {};
    this.appCreatorStudioSchema?.appInfo?.appFeatures.forEach((element: any) => {
      if (element && element == "EnableTenantService" || element == 'NavigationPage')
        appFeaturesFormObject[`${element}`] = new FormControl(true, [Validators.required]);
      else
        appFeaturesFormObject[`${element}`] = new FormControl(false, [Validators.required]);
      if (element)
        this.appFeaturesForm = new FormGroup(appFeaturesFormObject);
    });
  }

  private initializeAppFastlaneSecrets(): void {
    let appFastlaneSecretsFormObject: any = {};
    this.appCreatorStudioSchema?.appInfo?.appFastlaneSecrets.forEach((element: any) => {
      appFastlaneSecretsFormObject[`${element}`] = [''];
      if (element)
        this.appFastlaneSecretsForm = this.formBuilder.group(appFastlaneSecretsFormObject);
    });
  }

  private initializeAppThemes(): void {
    let appThemesFormsObject: any = {};
    console.log('initializeAppThemes', this.themesResultSchema);
    this.appCreatorStudioSchema?.appThemes?.sections.forEach((element: any) => {
      appThemesFormsObject[`${element}`] = new FormControl(JSON.stringify(this.themesResultSchema[`${element}`], null, 4), [
        Validators.required,
        Validators.minLength(100)
      ]);

      if (element) {
        this.appThemesForms = new FormGroup(appThemesFormsObject);
      }

    });
  }

  public submitFormData(): void {
    try {
      if (this.workItemForm.invalid) {
        for (const control of Object.keys(this.workItemForm.controls)) {
          console.log("control is", control);
          this.workItemForm.controls[control].markAsTouched();
        }
        return;
      }

      if (this.appInfoForm.invalid) {
        for (const control of Object.keys(this.appInfoForm.controls)) {
          console.log("control is", control);
          this.appInfoForm.controls[control].markAsTouched();
        }
        return;
      }

      if (this.appFeaturesForm.invalid) {
        for (const control of Object.keys(this.appFeaturesForm.controls)) {
          console.log("control is", control);
          this.appFeaturesForm.controls[control].markAsTouched();
        }
        return;
      }

      if (this.appFastlaneSecretsForm.invalid) {
        for (const control of Object.keys(this.appFastlaneSecretsForm.controls)) {
          console.log("control is", control);
          this.appFastlaneSecretsForm.controls[control].markAsTouched();
        }
        return;
      }

      if (this.appThemesForms.invalid) {
        for (const control of Object.keys(this.appThemesForms.controls)) {
          console.log("control is", control);
          this.appThemesForms.controls[control].markAsTouched();
        }
        return;
      }

      const finalAppJsonValue: object = {
        'appInfo': {
          'workItemInformation': this.workItemForm.value,
          'appInformation': this.appInfoForm.value,
          'appFeatures': this.appFeaturesForm.value,
          'appFastlaneSecrets': this.appFastlaneSecretsForm.value
        },
        'appThemes': {
          'backgrounds': this.encodeString(JSON.stringify(this.appThemesForms.value.backgrounds)),
          'colors': this.encodeString(JSON.stringify(this.appThemesForms.value.colors)),
          'fonts': this.encodeString(JSON.stringify(this.appThemesForms.value.fonts)),
        }
      };
      const finalAppsListJsonValues: object = {
        'createdBy': {
          'userEmail': (this.userInformation?.displayName) ? this.userInformation?.displayName : this.userInformation?.email,
          'userId': (this.userInformation?.uid) ? this.userInformation?.uid : '',
        },
        'createdOn': new Date().getTime(),
        'appName': (this.appInfoForm.value.AppName) ? this.appInfoForm.value.AppName : "",
        'projectName': (this.appInfoForm.value.ProjectName) ? this.appInfoForm.value.ProjectName : "",
        'isAppCreated': false,
        "appConfig": finalAppJsonValue
      };
      console.log('finalAppsListJsonValues is', finalAppsListJsonValues);

      if (this.appCreatorStudioSchema && this.appCreatorStudioSchema?.allowToUpdateAppsList) {
        this.updateAppList(finalAppsListJsonValues);
        this.resetForms();
        this.location.back();
      }
      else {
        this.showAlert("Information", "Updating Apps List is not Enabled for Now.", 'information');
      }
    } catch (error) {
      console.log("submitFormData catch error", error);
    }
  }

  private updateAppList(appsListJsonObject: object): void {
    if (appsListJsonObject) {
      console.log("appsListJsonObject", appsListJsonObject);
      this.dataService.updateCreatedAppsList(appsListJsonObject).subscribe((data) => {
        console.log("updateCreatedAppsList response is", data);
      });
    } else {
      console.log("Invalid updateAppList appsListJsonObject", appsListJsonObject);
    }
  }

  public resetForms(): void {
    window.location.reload();
  }

  private encodeString(stringValue: string) {
    return window.btoa(unescape(encodeURIComponent(stringValue)));
  }

  private async showAlert(title: string, message: string, type: string) {
    let alertPopup = await this.alert.create({
      header: title,
      message: message,
      cssClass: '',
      backdropDismiss: false,
      buttons: [{
        text: 'ok',
        handler: () => {
          this.alert.dismiss();
        }
      }]
    })
    await alertPopup.present();
  }

  async registerUser() {
    this.popOver.dismiss();
    let registerModal = await this.modal.create({
      component: RegistrationPage,
      backdropDismiss: false
    });
    await registerModal.present();
  }

  async showScssToJsonConvertorModel() {
    const modalWindow = await this.modal.create({
      component: ConvertCssJsonPage,
      backdropDismiss: true,
      showBackdrop: true
    });
    await modalWindow.present();
  }
}