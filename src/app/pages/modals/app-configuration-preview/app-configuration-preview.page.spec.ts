import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AppConfigurationPreviewPage } from './app-configuration-preview.page';

describe('AppConfigurationPreviewPage', () => {
  let component: AppConfigurationPreviewPage;
  let fixture: ComponentFixture<AppConfigurationPreviewPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(AppConfigurationPreviewPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
