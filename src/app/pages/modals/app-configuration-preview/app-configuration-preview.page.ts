import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import JSONEditor, { JSONEditorOptions } from 'jsoneditor';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';


@Component({
  selector: 'app-app-configuration-preview',
  templateUrl: './app-configuration-preview.page.html',
  styleUrls: ['./app-configuration-preview.page.scss'],
})
export class AppConfigurationPreviewPage implements OnInit, AfterViewInit {
  @ViewChild('appPreviewEditor', { static: false }) appPreviewEditor: ElementRef | undefined;

  jsonEditorOptions: JSONEditorOptions = {};
  jsonEditor: JSONEditor | null = null;

  showSkeleton: boolean = true;
  appListObject: any;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private location: Location
  ) { }

  ngOnInit() {
    this.processParamsData();
  }

  ngAfterViewInit(): void {
    // Adding timeout to load the HTML DOM Elements fully and then bind the values
    setTimeout(() => {
      if (this.appListObject) {
        this.initializeJSONEditor();
      }
    }, 3500);
  }

  public processParamsData(): void {
    this.route.queryParams.subscribe(() => {
      if (this.router.getCurrentNavigation()?.extras.state) {
        const stateParam: any = this.router.getCurrentNavigation()?.extras?.state;
        console.log("stateParam is", stateParam);
        this.appListObject = stateParam["appListObject"];

        // Decrypting encoded background, colors, fonts values
        const appThemesObject: any = this.appListObject?.appConfig?.appThemes;
        const appThemesBackgroundObject: any = this.decodeString(appThemesObject['backgrounds']);
        const appThemesColorObject: any = this.decodeString(appThemesObject['colors']);
        const appThemesFontsObject: any = this.decodeString(appThemesObject['fonts']);
        if (appThemesObject && appThemesBackgroundObject && appThemesColorObject && appThemesFontsObject) {
          this.appListObject.appConfig.appThemes.backgrounds = appThemesBackgroundObject;
          this.appListObject.appConfig.appThemes.colors = appThemesColorObject;
          this.appListObject.appConfig.appThemes.fonts = appThemesFontsObject;
        }

        console.log("Final this.appListObject", this.appListObject);
        this.showSkeleton = false;
      }
    });
  }

  public initializeJSONEditor() {
    console.log(`Initializing JSON Editor`);
    let element = this.appPreviewEditor?.nativeElement;
    //window.document.getElementById("appPreviewEditor");
    console.log(`element is ${element}`);
    if (element && this.appListObject) {
      console.log('inside initializeJSONEditor');
      const options: JSONEditorOptions = {
        mode: 'view',
        modes: ['view'],
        onChange: () => {
          this.appListObject = editor.get();
        }
      };
      let editor = new JSONEditor(element, options)
      editor.set(this.appListObject);
    }
  }

  public goBack(): void {
    this.location.back();
  }

  private decodeString(stringValue: string) {
    return JSON.parse(decodeURIComponent(escape(window.atob(stringValue))));
  }
}
