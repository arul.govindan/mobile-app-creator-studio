import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AppConfigurationPreviewPageRoutingModule } from './app-configuration-preview-routing.module';
import { AppConfigurationPreviewPage } from './app-configuration-preview.page';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AppConfigurationPreviewPageRoutingModule
  ],
  declarations: [AppConfigurationPreviewPage]
})
export class AppConfigurationPreviewPageModule {}
