import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../../../services/authentication/authentication-service';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-password-reset',
  templateUrl: './password-reset.page.html',
  styleUrls: ['./password-reset.page.scss'],
})

export class PasswordResetPage implements OnInit {
  constructor(public authService: AuthenticationService, private modal: ModalController) { }

  ngOnInit() { }
  cancel() {
    return this.modal.dismiss(null, 'cancel')
  }

}
