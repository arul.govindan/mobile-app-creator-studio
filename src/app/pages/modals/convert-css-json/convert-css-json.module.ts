import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ConvertCssJsonPageRoutingModule } from './convert-css-json-routing.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ConvertCssJsonPageRoutingModule
  ],
  declarations: []
})
export class ConvertCssJsonPageModule { }
