import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ConvertCssJsonPage } from './convert-css-json.page';

const routes: Routes = [
  {
    path: '',
    component: ConvertCssJsonPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ConvertCssJsonPageRoutingModule {}
