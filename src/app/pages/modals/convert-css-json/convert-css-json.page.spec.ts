import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ConvertCssJsonPage } from './convert-css-json.page';

describe('ConvertCssJsonPage', () => {
  let component: ConvertCssJsonPage;
  let fixture: ComponentFixture<ConvertCssJsonPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(ConvertCssJsonPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
