import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-convert-css-json',
  templateUrl: './convert-css-json.page.html',
  styleUrls: ['./convert-css-json.page.scss'],
})
export class ConvertCssJsonPage implements OnInit {
  cssDataString: string = '';
  jsonDataString: string = '';

  constructor(
    private modal: ModalController
  ) { }

  ngOnInit() {
  }

  public convertSCSStoJSON(): void {
    let parsedWiki = "";
    this.cssDataString.split('\n').forEach((wikiAttributeLine) => {
      wikiAttributeLine = wikiAttributeLine.replace(/^\s+|\s+$/gm, '')
        .replace(/^/, '"') // start each line with "
        .replace(/:(?!\/)/g, '":"') // replace all : with ":"
        .replace('":" {', '": {') // remove quotes before new object is declared
        .replace(/;(?!})/g, '",') // replace ; with ,
        .replace('"}",', '},') // remove unnecessary trailing quotes
        .replace(/ {4}/g, ''); // remove tabbing
      parsedWiki = parsedWiki + wikiAttributeLine;
    });
    parsedWiki = "{" + parsedWiki + "}";
    parsedWiki = parsedWiki.replace(/,}/g, '}'); // remove unnecessary trailing commas

    if (parsedWiki)
      this.jsonDataString = parsedWiki;
  }
  public cancelModel() {
    this.modal.dismiss();
  }
  public resetInputFields(): void {
    this.jsonDataString = '';
    this.cssDataString = '';
  }
}
