import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../services/authentication/authentication-service';
import { ModalController } from '@ionic/angular';
import { PasswordResetPage } from '../modals/password-reset/password-reset.page';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  appVersion: string = environment.version;

  constructor(
    public authService: AuthenticationService,
    public router: Router,
    public modal: ModalController,
  ) {
  }

  ngOnInit() {
  }

  logIn(email: { value: string; }, password: { value: string; }) {
    this.authService
      .SignIn(email.value, password.value)
      .then((res: any) => {
        console.log(res);
        if (this.authService.isEmailVerified) {
          this.router.navigate(['dashboard']);
        } else {
          window.alert('Email is not verified');
          return false;
        }
      })
      .catch((error: { message: any; }) => {
        window.alert(error.message);
      });
  }

  async showPasswordReset() {
    let passResetModal = await this.modal.create({
      component: PasswordResetPage,
      backdropDismiss: false
    });
    await passResetModal.present();
  }

  signInWithMicrosoft(): void {
    alert('Coming Soon');
  }
}
